﻿using Moq;
using SizeOfFiles.Core.Events;
using SizeOfFiles.Core.Interfaces;
using SizeOfFiles.Core.Services;
using System.IO;
using System.Linq;
using Xunit;

namespace SizeOfFiles.TestApp
{
    public class CountSizeServiceTests
    {
        [Fact]
        public void ProcessValieResult()
        {
            var fileProvider = new Mock<IFileProvider>();
            fileProvider.Setup(x => x.GetFileSize(It.IsAny<string>())).Returns(100);

            var direcotryProvider = new Mock<IDirectoryProvider>();
            direcotryProvider.Setup(x => x.GetFiles(It.IsAny<string>())).Returns(new string[] { "str1", "str1", "str1" });
            direcotryProvider.Setup(x => x.Exists(It.IsAny<string>())).Returns(true);
            var loger = new Mock<ILoger>().Object;

            var countSizeService = new CountSizeService(fileProvider.Object, direcotryProvider.Object, loger);
            countSizeService.CountSizeNotify += ChecAccountHandler;
            countSizeService.Process(string.Empty, new System.Threading.CancellationToken());
        }

        [Fact]
        public void ProcessInValieResult()
        {
            var fileProvider = new Mock<IFileProvider>();
            fileProvider.Setup(x => x.GetFileSize(It.IsAny<string>())).Returns(100);

            var direcotryProvider = new Mock<IDirectoryProvider>();
            direcotryProvider.Setup(x => x.GetFiles(It.IsAny<string>())).Returns(new string[0]);
            direcotryProvider.Setup(x => x.Exists(It.IsAny<string>())).Returns(true);
            var loger = new Mock<ILoger>();
            string expectedResult = string.Empty;
            loger.Setup(x => x.Info(It.IsAny<string>())).Callback<object>(s => expectedResult = s.ToString());

            var countSizeService = new CountSizeService(fileProvider.Object, direcotryProvider.Object, loger.Object);

            countSizeService.Process(string.Empty, new System.Threading.CancellationToken());
            loger.Verify(mock => mock.Info(It.IsAny<string>()), Times.Once());
            Assert.Equal("Count of files: 0;", expectedResult);
        }

        [Fact]
        public void ProcessExceptionResult()
        {
            var fileProvider = new Mock<IFileProvider>();

            var direcotryProvider = new Mock<IDirectoryProvider>();
            direcotryProvider.Setup(item => item.GetFiles(It.IsAny<string>())).Throws(new IOException());
            direcotryProvider.Setup(x => x.Exists(It.IsAny<string>())).Returns(true);
            var loger = new Mock<ILoger>();

            var countSizeService = new CountSizeService(fileProvider.Object, direcotryProvider.Object, loger.Object);
            var ex = Assert.Throws<IOException>(() => countSizeService.Process(string.Empty, new System.Threading.CancellationToken()));
            Assert.Equal("I/O error occurred.", ex.Message);
        }

        private void ChecAccountHandler(object sender, CountSizeEventArgs e)
        {
            Assert.Equal(3, e.FileSizes.Count);
            Assert.Equal("str1", e.FileSizes.First().FileName);
            Assert.Equal(100, e.FileSizes.First().Size);
        }
    }
}