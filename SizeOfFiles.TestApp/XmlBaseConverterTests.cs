﻿using Moq;
using SizeOfFiles.Core.Converters;
using SizeOfFiles.Core.Interfaces;
using SizeOfFiles.Core.Models;
using System.Collections.Generic;
using System.IO;
using Xunit;

namespace SizeOfFiles.TestApp
{
    public class XmlBaseConverterTests
    {
        [Fact]
        public void ConvertValieResult()
        {
            var fileProvider = new Mock<IFileProvider>();
            fileProvider.Setup(x => x.GetFileSize(It.IsAny<string>())).Returns(100);

            var loger = new Mock<ILoger>().Object;

            var xmlBaseConverter = new XmlBaseConverter(loger);
            var actual = xmlBaseConverter.Convert(GetFileSizeModels());

            var expected = @"<Files>
  <File>
    <FileName>str1</FileName>
    <Size>100</Size>
  </File>
  <File>
    <FileName>str2</FileName>
    <Size>200</Size>
  </File>
  <File>
    <FileName>str3</FileName>
    <Size>300</Size>
  </File>
</Files>";

            Assert.Equal(expected, actual.ToString());
        }

        [Fact]
        public void ConvertExceptionResult()
        {
            var fileProvider = new Mock<IFileProvider>();

            fileProvider.Setup(x => x.GetFileSize(It.IsAny<string>())).Returns(100);

            var loger = new Mock<ILoger>();
            loger.Setup(item => item.Info(It.IsAny<string>())).Throws(new IOException());

            var xmlService = new XmlBaseConverter(loger.Object);

            var ex = Assert.Throws<IOException>(() => xmlService.Convert(GetFileSizeModels()));
            Assert.Equal("I/O error occurred.", ex.Message);
        }

        private static List<FileSizeModel> GetFileSizeModels()
        {
            return new List<FileSizeModel> { new FileSizeModel { FileName = "str1", Size = 100 }, new FileSizeModel { FileName = "str2", Size = 200 }, new FileSizeModel { FileName = "str3", Size = 300 } };
        }
    }
}
