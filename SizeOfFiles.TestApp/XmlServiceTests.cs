﻿using Moq;
using SizeOfFiles.Core.Interfaces;
using SizeOfFiles.Core.Models;
using SizeOfFiles.Core.Services;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using Xunit;

namespace SizeOfFiles.TestApp
{
    public class XmlServiceTests
    {
        [Theory]
        [InlineData(true, "Appending into file: ;")]
        [InlineData(false, "Creating new file: ;")]
        public void ProcessValidResult(bool isFileExist, string actual)
        {
            var xmlConverter = new Mock<IXmlConverter>().Object;
            
            var fileProvider = new Mock<IFileProvider>();
            fileProvider.Setup(x => x.Exists(It.IsAny<string>())).Returns(isFileExist);

            var loger = new Mock<ILoger>();
            string expectedResult = string.Empty;
            loger.Setup(x => x.Info(It.IsAny<string>())).Callback<object>(s => expectedResult = s.ToString());

            var xmlProvider = new Mock<IXmlProvider>();
            TextReader tr = new StringReader("<Files><File><FileName>str1</FileName><Size>100</Size></File></Files>");
            xmlProvider.Setup(x => x.Load(It.IsAny<string>())).Returns(XDocument.Load(tr));

            var xmlService = new XmlService(fileProvider.Object, loger.Object, xmlConverter, xmlProvider.Object);

            xmlService.SaveResultToXmlFile(GetFileSizeModels(), string.Empty);
            Assert.Equal(actual, expectedResult);
        }

        [Fact]
        public void ProcessExceptionResult()
        {
            var xmlConverter = new Mock<IXmlConverter>().Object;
            var xmlProvider = new Mock<IXmlProvider>().Object;
            var fileProvider = new Mock<IFileProvider>();
            
            fileProvider.Setup(x => x.GetFileSize(It.IsAny<string>())).Returns(100);

            var loger = new Mock<ILoger>();
            loger.Setup(item => item.Info(It.IsAny<string>())).Throws(new IOException());

            var xmlService = new XmlService(fileProvider.Object, loger.Object, xmlConverter, xmlProvider);

            var ex = Assert.Throws<IOException>(() => xmlService.SaveResultToXmlFile(GetFileSizeModels(), string.Empty));
            Assert.Equal("I/O error occurred.", ex.Message);
        }

        private static List<FileSizeModel> GetFileSizeModels()
        {
            return new List<FileSizeModel> { new FileSizeModel { FileName = "str1", Size = 100 }, new FileSizeModel { FileName = "str2", Size = 200 }, new FileSizeModel { FileName = "str3", Size = 300 } };
        }
    }
}