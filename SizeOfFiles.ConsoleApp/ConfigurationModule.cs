﻿using Autofac;
using SizeOfFiles.Core.Converters;
using SizeOfFiles.Core.Interfaces;
using SizeOfFiles.Core.Providers;
using SizeOfFiles.Core.Services;

namespace SizeOfFiles.ConsoleApp
{
    internal class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new PhysicalFileProvider()).As<IFileProvider>();
            builder.Register(c => new PhysicalDirectoryProvider()).As<IDirectoryProvider>();
            builder.Register(c => new PhysicalXmlProvider()).As<IXmlProvider>();
            builder.Register(c => new Log4NetProvider(typeof(Program))).As<ILoger>();

            builder.Register(c => new XmlBaseConverter(c.Resolve<ILoger>())).As<IXmlConverter>();

            builder.Register(c => new XmlService(c.Resolve<IFileProvider>(), c.Resolve<ILoger>(), c.Resolve<IXmlConverter>(), c.Resolve<IXmlProvider>())).As<IXmlService>();
            builder.Register(c => new CountSizeService(c.Resolve<IFileProvider>(), c.Resolve<IDirectoryProvider>(), c.Resolve<ILoger>())).As<ICountSizeService>();
        }
    }
}