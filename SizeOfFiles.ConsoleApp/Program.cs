﻿using Autofac;
using log4net;
using log4net.Config;
using SizeOfFiles.Core.Events;
using SizeOfFiles.Core.Interfaces;
using System;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace SizeOfFiles.ConsoleApp
{
    internal class Program
    {
        private static string path = @"e:\temp\";

        private static readonly string ErrStr = @"error during operation {0}.
try again or contact your system administrator";

        private static IContainer container;

        private static void Main()
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));

            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterModule<ConfigurationModule>();
            container = containerBuilder.Build();

            Console.WriteLine("Enter 'E' for exit");
            Console.Write("Enter path: ");
            var userPath = Console.ReadLine();
            path = string.IsNullOrWhiteSpace(userPath) ? path : userPath;

            try
            {
                var countSizeService = container.Resolve<ICountSizeService>();
                countSizeService.CountSizeNotify += CountSizeEventArgs_ShowResult;

                var cancelTokenSource = new CancellationTokenSource();
                var token = cancelTokenSource.Token;
                Task.Run(() => countSizeService.Process(path, token));

                if (Console.ReadKey().Key == ConsoleKey.E)
                {
                    cancelTokenSource.Cancel();
                }
            }
            catch (Exception ex)
            {
                container.Resolve<ILoger>().Error(ex.Message, ex);
                Console.WriteLine(string.Format(ErrStr, nameof(ICountSizeService)));
            }

            Console.ReadLine();
        }

        private static void CountSizeEventArgs_ShowResult(object sender, CountSizeEventArgs e)
        {
            try
            {
                var xmlService = container.Resolve<IXmlService>();
                xmlService.SaveResultToXmlFile(e.FileSizes, Path.Combine(path + "console_info.xml"));

                foreach (var item in e.FileSizes)
                {
                    Console.WriteLine(item);
                }
            }
            catch (Exception ex)
            {
                container.Resolve<ILoger>().Error(ex.Message, ex);
                Console.WriteLine(string.Format(ErrStr, nameof(IXmlService)));
            }
        }
    }
}