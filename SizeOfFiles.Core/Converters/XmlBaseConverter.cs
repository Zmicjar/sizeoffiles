﻿using SizeOfFiles.Core.Interfaces;
using SizeOfFiles.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace SizeOfFiles.Core.Converters
{
    /// <summary>Xml Base Converter</summary>
    public class XmlBaseConverter : IXmlConverter
    {
        private readonly ILoger _loger;

        /// <summary>Initializes a new instance of the <see cref="XmlBaseConverter" /> class.</summary>
        /// <param name="loger">The loger.</param>
        public XmlBaseConverter(ILoger loger)
        {
            _loger = loger;
        }

        /// <summary>Gets the x element.</summary>
        /// <param name="items">The items.</param>
        /// <returns></returns>
        public XElement Convert(List<FileSizeModel> items)
        {
            _loger.Debug($"Start: {nameof(Convert)};");

            try
            {
                _loger.Info($"Size items: {items.Count};");
                var xmlElements = new XElement("Files", items.Select(i => new XElement("File", new XElement(nameof(i.FileName), i.FileName), new XElement(nameof(i.Size), i.Size))));

                _loger.Debug($"End: {nameof(Convert)};");
                return xmlElements;
            }
            catch (Exception ex)
            {
                _loger.Error(ex.Message, ex);
                throw;
            }
        }
    }
}