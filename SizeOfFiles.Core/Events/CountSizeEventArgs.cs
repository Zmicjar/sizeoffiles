﻿using SizeOfFiles.Core.Models;
using System;
using System.Collections.Generic;

namespace SizeOfFiles.Core.Events
{
    /// <summary>CountSizeEventArgs</summary>
    public class CountSizeEventArgs : EventArgs
    {
        /// <summary>Gets the file sizes.</summary>
        /// <value>The file sizes.</value>
        public List<FileSizeModel> FileSizes { get; private set; }

        /// <summary>Initializes a new instance of the <see cref="CountSizeEventArgs" /> class.</summary>
        /// <param name="fileSizes">The file sizes.</param>
        public CountSizeEventArgs(List<FileSizeModel> fileSizes)
        {
            FileSizes = fileSizes;
        }
    }
}