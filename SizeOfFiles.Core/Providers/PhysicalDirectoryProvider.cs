﻿using SizeOfFiles.Core.Interfaces;
using System.IO;

namespace SizeOfFiles.Core.Providers
{
    /// <summary>Physical Directory Provider</summary>
    public class PhysicalDirectoryProvider : IDirectoryProvider
    {
        /// <summary>Exists the specified path.</summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public bool Exists(string path)
        {
            return Directory.Exists(path);
        }

        /// <summary>Gets the files.</summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public string[] GetFiles(string path)
        {
            return Directory.GetFiles(path, "*", SearchOption.AllDirectories);
        }
    }
}