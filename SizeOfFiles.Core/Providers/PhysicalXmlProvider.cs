﻿using SizeOfFiles.Core.Interfaces;
using System.Xml.Linq;

namespace SizeOfFiles.Core.Providers
{
    /// <summary>Physical Xml Provider</summary>
    public class PhysicalXmlProvider : IXmlProvider
    {
        /// <summary>Saves the specified x element.</summary>
        /// <param name="xElement">The x element.</param>
        /// <param name="path">The path.</param>
        public void Save(XElement xElement, string path)
        {
            xElement.Save(path);
        }

        /// <summary>Saves the specified document.</summary>
        /// <param name="doc">The document.</param>
        /// <param name="path">The path.</param>
        public void Save(XDocument doc, string path)
        {
            doc.Save(path);
        }

        /// <summary>Loads the specified path.</summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public XDocument Load(string path)
        {
            return XDocument.Load(path);
        }
    }
}