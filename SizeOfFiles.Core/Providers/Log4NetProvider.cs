﻿using log4net;
using SizeOfFiles.Core.Interfaces;
using System;

namespace SizeOfFiles.Core.Providers
{
    /// <summary>Log4Net Provider</summary>
    public class Log4NetProvider : ILoger
    {
        private readonly ILog _log;

        /// <summary>Initializes a new instance of the <see cref="Log4NetProvider" /> class.</summary>
        /// <param name="type">The type.</param>
        public Log4NetProvider(Type type)
        {
            _log = LogManager.GetLogger(type);
        }

        /// <summary>Debugs the specified message.</summary>
        /// <param name="message">The message.</param>
        public void Debug(object message)
        {
            _log.Debug(message);
        }

        /// <summary>Debugs the specified message.</summary>
        /// <param name="message">The message.</param>
        /// <param name="exception">The exception.</param>
        public void Debug(object message, Exception exception)
        {
            _log.Debug(message, exception);
        }

        /// <summary>Errors the specified message.</summary>
        /// <param name="message">The message.</param>
        public void Error(object message)
        {
            _log.Error(message);
        }

        /// <summary>Errors the specified message.</summary>
        /// <param name="message">The message.</param>
        /// <param name="exception">The exception.</param>
        public void Error(object message, Exception exception)
        {
            _log.Error(message, exception);
        }

        /// <summary>Informations the specified message.</summary>
        /// <param name="message">The message.</param>
        public void Info(object message)
        {
            _log.Info(message);
        }

        /// <summary>Warns the specified message.</summary>
        /// <param name="message">The message.</param>
        public void Warn(object message)
        {
            _log.Warn(message);
        }

        /// <summary>Warns the specified message.</summary>
        /// <param name="message">The message.</param>
        /// <param name="exception">The exception.</param>
        public void Warn(object message, Exception exception)
        {
            _log.Warn(message, exception);
        }
    }
}