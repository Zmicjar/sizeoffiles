﻿using SizeOfFiles.Core.Interfaces;
using System.IO;

namespace SizeOfFiles.Core.Providers
{
    /// <summary>Physical File Provider</summary>
    public class PhysicalFileProvider : IFileProvider
    {
        /// <summary>Gets the size of the file.</summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public long GetFileSize(string path)
        {
            return new FileInfo(path).Length;
        }

        /// <summary>Exists the specified file name.</summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        public bool Exists(string fileName)
        {
            return File.Exists(fileName);
        }
    }
}