﻿namespace SizeOfFiles.Core.Models
{
    /// <summary>FileSize Model</summary>
    public class FileSizeModel
    {
        public string FileName { get; set; }
        public long Size { get; set; }

        public override string ToString()
        {
            return $"{nameof(FileName)} : {FileName}; {nameof(Size)} : {Size}";
        }
    }
}