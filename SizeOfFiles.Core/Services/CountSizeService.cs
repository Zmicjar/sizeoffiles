﻿using SizeOfFiles.Core.Events;
using SizeOfFiles.Core.Interfaces;
using SizeOfFiles.Core.Models;
using System;
using System.IO;
using System.Linq;
using System.Threading;

namespace SizeOfFiles.Core.Services
{
    /// <summary>CountSize Service</summary>
    public class CountSizeService : ICountSizeService
    {
        public event AccountHandler CountSizeNotify;

        public event EventHandler ProcessCompleted;

        private readonly IFileProvider _fileProvider;
        private readonly IDirectoryProvider _directoryProvider;
        private readonly ILoger _loger;

        private readonly int _batchSize = 3000;

        /// <summary>Initializes a new instance of the <see cref="CountSizeService" /> class.</summary>
        /// <param name="fileProvider">The file provider.</param>
        /// <param name="directoryProvider">The directory provider.</param>
        /// <param name="loger">The loger.</param>
        public CountSizeService(
            IFileProvider fileProvider,
            IDirectoryProvider directoryProvider,
            ILoger loger)
        {
            _fileProvider = fileProvider;
            _directoryProvider = directoryProvider;
            _loger = loger;
        }

        /// <summary>Processes the specified path.</summary>
        /// <param name="path">The path.</param>
        /// <param name="token">The token.</param>
        public void Process(string path, CancellationToken token)
        {
            _loger.Debug($"Start {nameof(Process)};");

            try
            {
                var filePaths = GetFiles(path);
                _loger.Info($"Count of files: {filePaths.Length};");

                if (filePaths.Length == 0)
                {
                    return;
                }

                var processed = 0;
                while (filePaths.Length > 0)
                {
                    var batch = filePaths.Take(_batchSize);
                    filePaths = filePaths.Skip(_batchSize).ToArray();

                    var result = batch.AsParallel().
                        Select(f => new FileSizeModel { FileName = Path.GetFileName(f), Size = _fileProvider.GetFileSize(f) }).
                        ToList();

                    CountSizeNotify?.Invoke(this, new CountSizeEventArgs(result));

                    processed += filePaths.Length;
                    _loger.Info($"{nameof(processed)}={processed};");

                    if (token.IsCancellationRequested)
                    {
                        _loger.Warn("Operation aborted by token");
                        return;
                    }
                }

                _loger.Debug($"End of {nameof(Process)};");
                ProcessCompleted?.Invoke(this, new EventArgs());
            }
            catch (Exception ex)
            {
                _loger.Error(ex.Message, ex);
                ProcessCompleted?.Invoke(this, new EventArgs());
                throw;
            }
        }

        /// <summary>Gets the files.</summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        private string[] GetFiles(string path)
        {
            if (!_directoryProvider.Exists(path))
            {
                return new string[0];
            }

            return _directoryProvider.GetFiles(path);
        }
    }
}