﻿using SizeOfFiles.Core.Interfaces;
using SizeOfFiles.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace SizeOfFiles.Core.Services
{
    /// <summary>Xml Service</summary>
    public class XmlService : IXmlService
    {
        private readonly IFileProvider _fileProvider;
        private readonly ILoger _loger;
        private readonly IXmlConverter _xmlConverter;
        private readonly IXmlProvider _xmlProvider;

        /// <summary>Initializes a new instance of the <see cref="XmlService" /> class.</summary>
        /// <param name="fileProvider">The file provider.</param>
        /// <param name="loger">The loger.</param>
        public XmlService(
            IFileProvider fileProvider,
            ILoger loger,
            IXmlConverter xmlConverter,
            IXmlProvider xmlProvider)
        {
            _fileProvider = fileProvider;
            _loger = loger;
            _xmlConverter = xmlConverter;
            _xmlProvider = xmlProvider;
        }

        /// <summary>Saves the x element to file.</summary>
        /// <param name="xElement">The x element.</param>
        /// <param name="path">The path.</param>
        public void SaveResultToXmlFile(List<FileSizeModel> items, string path)
        {
            _loger.Debug($"Start: {nameof(SaveResultToXmlFile)};");
            try
            {
                var xElement = _xmlConverter.Convert(items);
                if (_fileProvider.Exists(path))
                {
                    _loger.Info($"Appending into file: {path};");
                    XDocument doc = _xmlProvider.Load(path);
                    doc.Document.Descendants("File").LastOrDefault().AddAfterSelf(xElement);
                    _xmlProvider.Save(doc, path);
                }
                else
                {
                    _loger.Info($"Creating new file: {path};");
                    _xmlProvider.Save(xElement, path);
                }

                _loger.Debug($"End: {nameof(SaveResultToXmlFile)};");
            }
            catch (Exception ex)
            {
                _loger.Error(ex.Message, ex);
                throw;
            }
        }
    }
}