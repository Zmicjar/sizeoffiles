﻿using SizeOfFiles.Core.Models;
using System.Collections.Generic;
using System.Xml.Linq;

namespace SizeOfFiles.Core.Interfaces
{
    /// <summary>IXmlConverter</summary>
    public interface IXmlConverter
    {
        /// <summary>Converts the specified items.</summary>
        /// <param name="items">The items.</param>
        /// <returns>XElement</returns>
        XElement Convert(List<FileSizeModel> items);
    }
}