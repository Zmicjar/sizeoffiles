﻿using SizeOfFiles.Core.Models;
using System.Collections.Generic;

namespace SizeOfFiles.Core.Interfaces
{
    /// <summary>IXmlService</summary>
    public interface IXmlService
    {
        /// <summary>Saves the x element to file.</summary>
        /// <param name="xElement">The x element.</param>
        /// <param name="path">The path.</param>
        void SaveResultToXmlFile(List<FileSizeModel> items, string path);
    }
}