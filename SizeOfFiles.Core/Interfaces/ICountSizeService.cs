﻿using System;
using System.Threading;

namespace SizeOfFiles.Core.Interfaces
{
    /// <summary>ICountSizeService</summary>
    public interface ICountSizeService
    {
        /// <summary>Occurs when [notify].</summary>
        event AccountHandler CountSizeNotify;

        /// <summary>Occurs when [process completed].</summary>
        event EventHandler ProcessCompleted;

        /// <summary>Processes the specified path.</summary>
        /// <param name="path">The path.</param>
        /// <param name="token">The token.</param>
        void Process(string path, CancellationToken token);
    }
}