﻿namespace SizeOfFiles.Core.Interfaces
{
    /// <summary>IFileProvider</summary>
    public interface IFileProvider
    {
        /// <summary>Gets the size of the file.</summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        long GetFileSize(string path);

        /// <summary>Exists the specified file name.</summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        bool Exists(string fileName);
    }
}