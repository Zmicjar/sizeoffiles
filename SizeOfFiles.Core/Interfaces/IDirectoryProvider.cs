﻿namespace SizeOfFiles.Core.Interfaces
{
    /// <summary>IDirectoryProvider</summary>
    public interface IDirectoryProvider
    {
        /// <summary>Gets the files.</summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        string[] GetFiles(string path);

        /// <summary>Existses the specified path.</summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        bool Exists(string path);
    }
}