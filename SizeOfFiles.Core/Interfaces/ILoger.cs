﻿using System;

namespace SizeOfFiles.Core.Interfaces
{
    /// <summary>ILoger</summary>
    public interface ILoger
    {
        /// <summary>Debugs the specified message.</summary>
        /// <param name="message">The message.</param>
        void Debug(object message);

        /// <summary>Debugs the specified message.</summary>
        /// <param name="message">The message.</param>
        /// <param name="exception">The exception.</param>
        void Debug(object message, Exception exception);

        /// <summary>Errors the specified message.</summary>
        /// <param name="message">The message.</param>
        void Error(object message);

        /// <summary>Errors the specified message.</summary>
        /// <param name="message">The message.</param>
        /// <param name="exception">The exception.</param>
        void Error(object message, Exception exception);

        /// <summary>Informations the specified message.</summary>
        /// <param name="message">The message.</param>
        void Info(object message);

        /// <summary>Warns the specified message.</summary>
        /// <param name="message">The message.</param>
        void Warn(object message);

        /// <summary>Warns the specified message.</summary>
        /// <param name="message">The message.</param>
        /// <param name="exception">The exception.</param>
        void Warn(object message, Exception exception);
    }
}