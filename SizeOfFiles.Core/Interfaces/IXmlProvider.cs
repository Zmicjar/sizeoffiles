﻿using System.Xml.Linq;

namespace SizeOfFiles.Core.Interfaces
{
    /// <summary>IXmlProvider</summary>
    public interface IXmlProvider
    {
        /// <summary>Saves the specified x element.</summary>
        /// <param name="xElement">The x element.</param>
        /// <param name="path">The path.</param>
        void Save(XElement xElement, string path);

        /// <summary>Saves the specified document.</summary>
        /// <param name="doc">The document.</param>
        /// <param name="path">The path.</param>
        void Save(XDocument doc, string path);

        /// <summary>Loads the specified path.</summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        XDocument Load(string path);
    }
}