﻿using SizeOfFiles.Core.Events;

namespace SizeOfFiles.Core
{
    /// <summary>AccountHandler delegate</summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The Event Argument<see cref="CountSizeEventArgs" /> instance containing the event data.</param>
    public delegate void AccountHandler(object sender, CountSizeEventArgs e);
}