﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace SizeOfFiles.App
{
    public class StringToTypeConverter : TypeConverter
    {
        public override object ConvertTo(ITypeDescriptorContext context,
                                     CultureInfo culture, object value,
                                     Type destinationType)
        {
            return Type.GetType(value.ToString());
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            return Type.GetType(value.ToString());
        }

        public Type ConvertTo(string value)
        {
            return Type.GetType(value);
        }
    }
}