﻿using Microsoft.Practices.Unity.Configuration;
using SizeOfFiles.Core.Events;
using SizeOfFiles.Core.Interfaces;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Unity;

namespace SizeOfFiles.App
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly string ErrStr = @"error during operation {0}.
try again or contact your system administrator";
      
        private static CancellationTokenSource cancelTokenSource = new CancellationTokenSource();
        private UnityContainer container;
        private string _path;
        private ICountSizeService countSizeService;

        public MainWindow()
        {
            InitializeComponent();
            container = new UnityContainer();

            container.LoadConfiguration();
            countSizeService = container.Resolve<ICountSizeService>();
            countSizeService.CountSizeNotify += CountSizeEventArgs_ShowResult;
            countSizeService.ProcessCompleted += ProcessCompleted;
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _path = PathTextBox.Text;
                if (string.IsNullOrWhiteSpace(_path))
                {
                    MessageBox.Show("Enter path;");
                }
                else
                {
                    cancelTokenSource = new CancellationTokenSource();
                    StartButton.IsEnabled = false;
                    Task.Run(() => countSizeService.Process(_path, cancelTokenSource.Token));
                }
            }
            catch (Exception ex)
            {
                container.Resolve<ILoger>().Error(ex.Message, ex);
                MessageBox.Show(string.Format(ErrStr, nameof(ICountSizeService)));
            }
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            cancelTokenSource.Cancel();
            StartButton.IsEnabled = true;
        }

        private void CountSizeEventArgs_ShowResult(object sender, CountSizeEventArgs e)
        {
            try
            {
                var xmlService = container.Resolve<IXmlService>();
                xmlService.SaveResultToXmlFile(e.FileSizes, Path.Combine(_path, "wpf_info.xml"));
                Dispatcher.Invoke(() =>
                {
                    InfoDataGrid.ItemsSource = e.FileSizes;
                });
            }
            catch (Exception ex)
            {
                container.Resolve<ILoger>().Error(ex.Message, ex);
                MessageBox.Show(string.Format(ErrStr, nameof(IXmlService)));
            }
        }

        private void ProcessCompleted(object sender, EventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                StartButton.IsEnabled = true;
            });
        }

        private void OpenButton_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}